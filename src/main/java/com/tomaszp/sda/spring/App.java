package com.tomaszp.sda.spring;

import com.tomaszp.sda.spring.car.MySportsCar;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        ApplicationContext context = new AnnotationConfigApplicationContext("com.tomaszp.sda.spring");
        MySportsCar mySportsCar = (MySportsCar) context.getBean("mySportsCar");
        mySportsCar.drive();
    }
}
