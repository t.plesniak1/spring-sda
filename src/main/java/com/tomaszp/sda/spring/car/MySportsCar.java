package com.tomaszp.sda.spring.car;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@PropertySource("classpath:spring.properties")
@Component
public class MySportsCar {

    @Value("${mySportsCar.make}")
    private String make;
    @Value("${mySportsCar.model}")
    private String model;

    private Body body;
    private Brakes brakes;
    private Engine engine;
    private Suspension suspension;
    private Tires tires;

    public MySportsCar(Body body, Brakes brakes, Engine engine, Suspension suspension, Tires tires) {
        this.body = body;
        this.brakes = brakes;
        this.engine = engine;
        this.suspension = suspension;
        this.tires = tires;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Brakes getBrakes() {
        return brakes;
    }

    public void setBrakes(Brakes brakes) {
        this.brakes = brakes;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Suspension getSuspension() {
        return suspension;
    }

    public void setSuspension(Suspension suspension) {
        this.suspension = suspension;
    }

    public Tires getTires() {
        return tires;
    }

    public void setTires(Tires tires) {
        this.tires = tires;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void drive() {
        System.out.println("Accelerating to " + engine.accelerate(10));
        tires.rollingOnRoad();
        brakes.brake();
    }
}
