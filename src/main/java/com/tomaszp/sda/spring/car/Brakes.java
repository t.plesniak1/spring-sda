package com.tomaszp.sda.spring.car;

public interface Brakes {

    void brake();
}
