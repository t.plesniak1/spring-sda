package com.tomaszp.sda.spring.car;

import org.springframework.stereotype.Component;

@Component
public class CabrioletBody implements Body {
    @Override
    public void openDoor(int num) {
        System.out.println("Open " + num +" door" );
    }

    @Override
    public void openWindow(int num) {
        System.out.println("Open " + num +" window" );
    }
}
