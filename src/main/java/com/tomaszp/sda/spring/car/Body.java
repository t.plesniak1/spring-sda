package com.tomaszp.sda.spring.car;

public interface Body {

    void openDoor(int num);
    void openWindow(int num);
}
