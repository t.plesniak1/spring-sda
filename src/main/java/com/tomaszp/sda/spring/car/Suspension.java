package com.tomaszp.sda.spring.car;

public interface Suspension {

   void suppress();
}
