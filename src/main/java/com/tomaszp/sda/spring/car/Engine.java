package com.tomaszp.sda.spring.car;

public interface Engine {

    int accelerate(int throttleOpen);
}
