package com.tomaszp.sda.spring.car;

import org.springframework.stereotype.Component;

@Component
public class GarrettTurbocharger implements Turbocharger {

    private int pressure;

    @Override
    public int compressAir(int rpm) {
        return pressure;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }
}
