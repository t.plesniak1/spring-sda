package com.tomaszp.sda.spring.car;

import org.springframework.stereotype.Component;

@Component
public class PetrolEngine implements Engine {

    private Turbocharger turbocharger;

    @Override
    public int accelerate(int throttleOpen) {
        return throttleOpen * 10;
    }

    public PetrolEngine(Turbocharger turbocharger) {
        this.turbocharger = turbocharger;
    }
}
