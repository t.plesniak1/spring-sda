package com.tomaszp.sda.spring.car;

public interface Turbocharger {

    int compressAir(int rpm);
}
