package com.tomaszp.sda.spring.car;

import org.springframework.stereotype.Component;

@Component
public class SportSuspension implements Suspension {
    @Override
    public void suppress() {
        System.out.println("Sport suspension");
    }
}
