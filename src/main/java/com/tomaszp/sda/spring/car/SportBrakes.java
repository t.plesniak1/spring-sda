package com.tomaszp.sda.spring.car;

import org.springframework.stereotype.Component;

@Component
public class SportBrakes implements Brakes {
    @Override
    public void brake() {
        System.out.println("trying stop the car");
    }
}
