package com.tomaszp.sda.spring.car;

import org.springframework.stereotype.Component;

@Component
public class PirelliSportsTires implements Tires {
    @Override
    public void rollingOnRoad() {
        System.out.println("moving on the road");
    }
}
